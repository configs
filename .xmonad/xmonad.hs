import XMonad
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig
import System.IO
 
myManageHook = composeAll
    [ className =? "Gimp"    --> doFloat
    , className =? "MPlayer" --> doFloat
    , className =? "Firefox" --> doF (W.shift "www")
    ]

main = do
    xmproc <- spawnPipe "/usr/local/bin/xmobar /home/mixer/.xmobarrc"
    xmonad $ defaultConfig
        { manageHook = manageDocks <+> myManageHook <+> manageHook defaultConfig
        , layoutHook = avoidStruts  $  layoutHook defaultConfig
	, workspaces = ["dev", "www", "im", "media", "5", "6", "7", "8", "9"]
        , logHook = dynamicLogWithPP $ defaultPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "#e0e0e0" "" . shorten 50
                        , ppCurrent = xmobarColor "#b6e77d" ""
--                        , ppHidden = pad
                        , ppHiddenNoWindows = xmobarColor "#444444" ""
                        , ppSep = " | "
--                        , ppWsSep = ""
                        }
--	, font = "-*-terminus-medium-r-normal-*-14-*-*-*-*-*-*"
	, focusFollowsMouse = False
	, modMask = mod4Mask
	, terminal = "urxvt"
	, focusedBorderColor = "#99bb99"
	, normalBorderColor = "#444444"
	} `additionalKeys` 
	[ ((mod4Mask, xK_f), spawn "firefox")
	]

